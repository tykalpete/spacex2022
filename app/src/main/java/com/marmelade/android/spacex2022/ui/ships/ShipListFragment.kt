package com.marmelade.android.spacex2022.ui.ships

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.marmelade.android.spacex2022.ui.base.BaseFragment
import com.marmelade.android.spacex2022.databinding.FragmentShipListBinding
import com.marmelade.android.spacex2022.ui.detail.ShipDetailActivity
import com.marmelade.android.spacex2022.ui.ships.adapter.ShipListAdapter
import com.marmelade.android.spacex2022.utils.isPagingInErrorState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject


/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
@AndroidEntryPoint
class ShipListFragment : BaseFragment<FragmentShipListBinding>() {

    @Inject
    lateinit var shipListAdapter: ShipListAdapter

    override val viewModel by viewModels<ShipListViewModel>()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.swipeRefreshLayout?.setOnRefreshListener { shipListAdapter.refresh() }
        setUpShipList()
        observeData()
    }

    override fun setViewBinding(
        inflater: LayoutInflater, container: ViewGroup?
    ): FragmentShipListBinding =
        FragmentShipListBinding.inflate(layoutInflater, container, false)

    private fun observeData() {
        viewModel.shipList
            .onEach { shipListAdapter.submitData(it) }
            .launchIn(lifecycleScope)
    }

    private fun setUpShipList() {
        shipListAdapter.doOnClick = { id ->
            startActivity(
                ShipDetailActivity.getStartIntent(
                    context = requireContext(),
                    id = id
                )
            )
        }
        binding?.list?.apply {
            layoutManager = LinearLayoutManager(this.context)
            adapter = shipListAdapter
        }
        shipListAdapter.addLoadStateListener { loadState ->
            binding?.swipeRefreshLayout?.isRefreshing = loadState.refresh is LoadState.Loading
            val error = loadState.isPagingInErrorState()?.error
            if (error != null) {
                viewModel.errorEvent.value = error
            }
            val isDataEmpty = (error != null || loadState.append.endOfPaginationReached)
                && shipListAdapter.itemCount == 0
            binding?.emptyStateImage?.isVisible = isDataEmpty
            binding?.emptyStateText?.isVisible = isDataEmpty
            if (isDataEmpty) {
                binding?.emptyStateImage?.playAnimation()
            } else {
                binding?.emptyStateImage?.cancelAnimation()
            }
        }
    }
}