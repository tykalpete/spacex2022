package com.marmelade.android.spacex2022.logic.di.modules

import com.marmelade.android.spacex2022.BuildConfig
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Named


/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
@InstallIn(SingletonComponent::class)
@Module
class AppModule {

    @Provides
    @Named("apiUrl")
    fun provideApiUrl(): String = BuildConfig.API_BASE_URL
}