package com.marmelade.android.spacex2022.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.marmelade.android.spacex2022.R
import com.marmelade.android.spacex2022.ui.base.BaseOnlyViewActivity
import com.marmelade.android.spacex2022.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint


/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
@AndroidEntryPoint
class MainActivity : BaseOnlyViewActivity<ActivityMainBinding>() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupBottomNavigation()
    }

    override fun setViewBinding(inflater: LayoutInflater): ActivityMainBinding =
        ActivityMainBinding.inflate(layoutInflater)

    private fun setupBottomNavigation() {
        val navController = findNavController(R.id.mainNavHostFragment)
        binding.mainBottomNavigation.setupWithNavController(navController)
    }
}