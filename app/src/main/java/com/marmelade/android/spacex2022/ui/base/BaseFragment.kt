package com.marmelade.android.spacex2022.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding
import com.marmelade.android.spacex2022.logic.handleError
import com.marmelade.android.spacex2022.utils.createSnackBar


/**
 * Base parent for fragments
 *
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
abstract class BaseFragment<T> : Fragment() where T : ViewBinding {

    protected abstract val viewModel: BaseViewModel
    protected var binding: T? = null
        private set
    protected var snackBarView: View? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        this.binding = this.setViewBinding(inflater, container)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.errorEvent.observe(viewLifecycleOwner) {
            showSnackBar(requireContext().handleError(it))
        }
    }

    @CallSuper
    override fun onDestroyView() {
        snackBarView = null
        binding = null
        super.onDestroyView()
    }

    abstract fun setViewBinding(inflater: LayoutInflater, container: ViewGroup?): T

    protected fun showSnackBar(message: String) {
        val rootView = snackBarView ?: binding?.root ?: return
        rootView.createSnackBar(message)?.show()
    }
}