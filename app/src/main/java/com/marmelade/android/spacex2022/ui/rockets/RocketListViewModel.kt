package com.marmelade.android.spacex2022.ui.rockets

import com.marmelade.android.spacex2022.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
@HiltViewModel
class RocketListViewModel @Inject constructor() : BaseViewModel()