package com.marmelade.android.spacex2022.ui.detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.marmelade.android.entities.Resource
import com.marmelade.android.entities.Ship
import com.marmelade.android.repository.ShipRepository
import com.marmelade.android.spacex2022.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
@HiltViewModel
class ShipDetailViewModel @Inject constructor(
    private val shipRepository: ShipRepository
) : BaseViewModel() {

    val ship = MutableLiveData<Ship>()

    fun getShip(id: String?) {
        id ?: return kotlin.run {
            errorEvent.value = UnknownError()
        }
        viewModelScope.launch {
            when (val resourceData = shipRepository.getShip(id = id)) {
                is Resource.Success -> {
                    ship.value = resourceData.data.ship ?: return@launch
                }
                is Resource.Error -> {
                    errorEvent.value = resourceData.throwable
                }
                else -> Unit
            }
        }
    }
}