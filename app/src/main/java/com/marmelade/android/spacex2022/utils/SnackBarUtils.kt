package com.marmelade.android.spacex2022.utils

import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.marmelade.android.spacex2022.R


/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */

fun View.createSnackBar(message: String? = null): Snackbar? {
    val snackBar = Snackbar.make(this, message ?: return null, Snackbar.LENGTH_LONG)
    val textView = snackBar.view
        .findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
    textView.apply {
        setTextAppearance(R.style.SnackBarTextStyle)
        setTextColor(ContextCompat.getColor(context, R.color.white))
        maxLines = 6
    }
    snackBar.view.setBackgroundResource(R.drawable.background_snackbar)
    return snackBar
}