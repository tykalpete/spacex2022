package com.marmelade.android.spacex2022.ui.ships

import com.marmelade.android.repository.ShipRepository
import com.marmelade.android.spacex2022.ui.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
@HiltViewModel
class ShipListViewModel @Inject constructor(
    shipRepository: ShipRepository
) : BaseViewModel() {

    val shipList = shipRepository
        .getShipListPager()
        .flow
}