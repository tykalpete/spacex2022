package com.marmelade.android.spacex2022.ui.base

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.marmelade.android.spacex2022.utils.createSnackBar


/**
 * Base parent for activities
 *
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
abstract class BaseActivity<T> : AppCompatActivity() where T : ViewBinding {

    protected abstract val viewModel: BaseViewModel
    protected lateinit var binding: T
        private set
    protected var snackBarView: View? = null


    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        binding = setViewBinding(layoutInflater)
        setContentView(binding.root)
    }

    abstract fun setViewBinding(inflater: LayoutInflater): T

    protected fun showSnackBar(message: String) {
        val rootView = snackBarView ?: binding.root
        rootView.createSnackBar(message)?.show()
    }
}