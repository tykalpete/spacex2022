package com.marmelade.android.spacex2022.ui.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import androidx.activity.viewModels
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.marmelade.android.entities.Ship
import com.marmelade.android.spacex2022.ui.base.BaseActivity
import com.marmelade.android.spacex2022.databinding.ActivityShipDetailBinding
import com.marmelade.android.spacex2022.logic.handleError
import dagger.hilt.android.AndroidEntryPoint


/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
@AndroidEntryPoint
class ShipDetailActivity : BaseActivity<ActivityShipDetailBinding>() {
    companion object {
        private const val SHIP_ID_EXTRAS = "extras.ship_id"
        private const val IMAGE_FADE_DURATION_MS = 200

        fun getStartIntent(context: Context, id: String) =
            Intent(context, ShipDetailActivity::class.java)
                .putExtra(SHIP_ID_EXTRAS, id)
    }

    override val viewModel by viewModels<ShipDetailViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        observeData()
        viewModel.getShip(id = getShipId())
    }

    override fun setViewBinding(inflater: LayoutInflater): ActivityShipDetailBinding =
        ActivityShipDetailBinding.inflate(layoutInflater)

    private fun observeData() {
        viewModel.errorEvent.observe(this) { showSnackBar(handleError(it)) }
        viewModel.ship.observe(this) { setShipData(it) }
    }

    private fun setShipData(ship: Ship) {
        binding.title.text = ship.name
        Glide.with(binding.image)
            .load(ship.image)
            .centerCrop()
            .transition(DrawableTransitionOptions.withCrossFade(IMAGE_FADE_DURATION_MS))
            .into(binding.image)
    }

    private fun getShipId(): String? = intent.getStringExtra(SHIP_ID_EXTRAS)
}