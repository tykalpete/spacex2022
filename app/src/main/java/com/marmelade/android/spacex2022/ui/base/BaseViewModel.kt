package com.marmelade.android.spacex2022.ui.base

import androidx.lifecycle.ViewModel
import com.marmelade.android.spacex2022.utils.SingleLiveEvent


/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
open class BaseViewModel : ViewModel() {

    val errorEvent = SingleLiveEvent<Throwable>()
}