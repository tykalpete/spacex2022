package com.marmelade.android.spacex2022.ui.base

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding


/**
 * Base parent for activities without ViewModel
 *
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
abstract class BaseOnlyViewActivity<T> : AppCompatActivity() where  T : ViewBinding {

    protected lateinit var binding: T
        private set


    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        val binding = setViewBinding(layoutInflater)
        this.binding = binding
        setContentView(binding.root)
    }

    abstract fun setViewBinding(inflater: LayoutInflater): T
}