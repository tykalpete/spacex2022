package com.marmelade.android.spacex2022.logic

import android.content.Context
import java.net.UnknownHostException


/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
fun Context.handleError(throwable: Throwable): String {
    return getString(
        when (throwable) {
            is UnknownHostException -> com.marmelade.android.resources.R.string.error_no_connection
            else -> com.marmelade.android.resources.R.string.error_unknown_error
        }
    )
}