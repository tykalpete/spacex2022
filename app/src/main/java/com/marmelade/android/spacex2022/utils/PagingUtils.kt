package com.marmelade.android.spacex2022.utils

import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState


/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */

fun CombinedLoadStates.isPagingInErrorState(): LoadState.Error? {
    return when {
        prepend is LoadState.Error -> prepend as LoadState.Error
        append is LoadState.Error -> append as LoadState.Error
        refresh is LoadState.Error -> refresh as LoadState.Error
        else -> null
    }
}