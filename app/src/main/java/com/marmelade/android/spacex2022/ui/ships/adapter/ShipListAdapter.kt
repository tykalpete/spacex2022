package com.marmelade.android.spacex2022.ui.ships.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.marmelade.android.entities.Ship
import com.marmelade.android.spacex2022.databinding.RowShipBinding
import javax.inject.Inject


/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
class ShipListAdapter @Inject constructor() :
    PagingDataAdapter<Ship, ShipListAdapter.ShipListViewHolder>(
        object : DiffUtil.ItemCallback<Ship>() {
            override fun areItemsTheSame(oldItem: Ship, newItem: Ship): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Ship, newItem: Ship): Boolean {
                return oldItem == newItem
            }
        }) {
    companion object {
        private const val IMAGE_FADE_DURATION_MS = 200
    }

    var doOnClick: ((id: String) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShipListViewHolder {
        val binding = RowShipBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ShipListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ShipListViewHolder, position: Int) {
        val item = getItem(position)
        if (item != null) {
            val isDividerVisible = (itemCount - 1) != position
            holder.bind(
                ship = item,
                isDividerVisible = isDividerVisible
            )
        }
    }

    inner class ShipListViewHolder(
        private val binding: RowShipBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(ship: Ship, isDividerVisible: Boolean) {
            binding.divider.isVisible = isDividerVisible
            binding.title.text = ship.name.orEmpty()
            val yearValue = ship.year?.toString().orEmpty()
            binding.year.isVisible = yearValue.isNotEmpty()
            binding.year.text = yearValue
            Glide.with(binding.image)
                .load(ship.image)
                .centerCrop()
                .transition(DrawableTransitionOptions.withCrossFade(IMAGE_FADE_DURATION_MS))
                .into(binding.image)
            binding.root.setOnClickListener { doOnClick?.invoke(ship.id) }
        }
    }
}