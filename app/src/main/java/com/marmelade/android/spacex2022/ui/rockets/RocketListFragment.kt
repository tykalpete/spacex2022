package com.marmelade.android.spacex2022.ui.rockets

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.marmelade.android.spacex2022.ui.base.BaseFragment
import com.marmelade.android.spacex2022.databinding.FragmentRocketListBinding
import dagger.hilt.android.AndroidEntryPoint


/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
@AndroidEntryPoint
class RocketListFragment : BaseFragment<FragmentRocketListBinding>() {

    override val viewModel by viewModels<RocketListViewModel>()

    override fun setViewBinding(
        inflater: LayoutInflater, container: ViewGroup?
    ): FragmentRocketListBinding =
        FragmentRocketListBinding.inflate(layoutInflater, container, false)
}