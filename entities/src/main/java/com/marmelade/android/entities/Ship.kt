package com.marmelade.android.entities

import com.squareup.moshi.Json

/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
data class Ship(
    val id: String,
    val name: String?,
    val image: String?,
    @Json(name = "year_built")
    val year: Int?
)