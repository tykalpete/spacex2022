package com.marmelade.android.entities.response

import com.marmelade.android.entities.Ship

/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
data class ShipListResponse(
    val ships: List<Ship>? = emptyList()
)

data class ShipIdResponse(
    val ship: Ship?
)