package com.marmelade.android.entities


/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
sealed class Resource<out T> {
    data class Success<out T>(val data: T) : Resource<T>()
    data class Error(val throwable: Throwable = UnknownError()) : Resource<Nothing>()
    object Loading : Resource<Nothing>()
}