package com.marmelade.android.entities.response

/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
data class DataResponse<T>(
    val data: T
)