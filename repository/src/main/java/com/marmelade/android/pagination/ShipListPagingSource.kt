package com.marmelade.android.pagination

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.marmelade.android.entities.Resource
import com.marmelade.android.entities.Ship
import com.marmelade.android.repository.ShipRepository
import javax.inject.Inject


/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
class ShipListPagingSource @Inject constructor(
    private val shipRepository: ShipRepository
) : PagingSource<Long, Ship>() {
    companion object {
        const val PAGE_SIZE = 10
    }

    override suspend fun load(params: LoadParams<Long>): LoadResult<Long, Ship> {
        val resource = shipRepository.getShips(
            offset = params.key?.toInt() ?: 0,
            limit = PAGE_SIZE
        )
        return when (resource) {
            is Resource.Success -> {
                val data = resource.data.ships ?: emptyList()
                val dataCount = data.size
                LoadResult.Page(
                    data = data,
                    prevKey = params.key,
                    nextKey =
                    if (dataCount < PAGE_SIZE) null
                    else (params.key ?: 0) + dataCount
                )
            }
            is Resource.Error -> {
                LoadResult.Error(resource.throwable)
            }
            else -> {
                LoadResult.Error(UnknownError())
            }
        }
    }

    override fun getRefreshKey(state: PagingState<Long, Ship>): Long? = null
}