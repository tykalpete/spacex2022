package com.marmelade.android.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.marmelade.android.api.Api
import com.marmelade.android.entities.Resource
import com.marmelade.android.entities.Ship
import com.marmelade.android.entities.response.ShipIdResponse
import com.marmelade.android.entities.response.ShipListResponse
import com.marmelade.android.pagination.ShipListPagingSource
import dagger.Reusable
import javax.inject.Inject
import javax.inject.Provider


/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
@Reusable
class ShipRepository @Inject constructor(
    private val api: Api,
    private val shipListPagingSource: Provider<ShipListPagingSource>
) {
    fun getShipListPager(): Pager<Long, Ship> =
        Pager(
            config = PagingConfig(
                pageSize = ShipListPagingSource.PAGE_SIZE,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { shipListPagingSource.get() }
        )

    suspend fun getShips(offset: Int, limit: Int): Resource<ShipListResponse> {
        return api.getShips(
            offset = offset,
            limit = limit
        )
    }

    suspend fun getShip(id: String): Resource<ShipIdResponse> {
        return api.getShip(
            id = id
        )
    }
}