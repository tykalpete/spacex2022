package com.marmelade.android.api

import com.marmelade.android.entities.Resource
import com.marmelade.android.entities.response.ShipIdResponse
import com.marmelade.android.entities.response.ShipListResponse
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton


/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
@Singleton
class Api @Inject constructor(
    client: OkHttpClient,
    moshi: Moshi,
    @Named("apiUrl")
    apiUrl: String
) : ApiCore(client, moshi, apiUrl) {

    suspend fun getShips(
        offset: Int, limit: Int
    ): Resource<ShipListResponse> {
        return request(
            operation = ShipListQuery(
                offset = offset,
                limit = limit
            ),
            modelClass = ShipListResponse::class.java
        )
    }

    suspend fun getShip(
        id: String
    ): Resource<ShipIdResponse> {
        return request(
            operation = ShipIdQuery(
                id = id
            ),
            modelClass = ShipIdResponse::class.java
        )
    }
}