package com.marmelade.android.api

import com.apollographql.apollo3.api.Operation
import com.apollographql.apollo3.api.composeJsonRequest
import com.apollographql.apollo3.api.json.buildJsonString
import com.marmelade.android.entities.Resource
import com.marmelade.android.entities.response.DataResponse
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import timber.log.Timber


/**
 * @author Petr Tykal <tykal.pete@gmail.com>
 */
open class ApiCore constructor(
    private val client: OkHttpClient,
    private val moshi: Moshi,
    private val apiUrl: String
) {
    companion object {
        private val JSON: MediaType = "application/json; charset=utf-8".toMediaType()
    }

    protected suspend fun <M> request(
        operation: Operation<*>, modelClass: Class<M>
    ): Resource<M> = try {
        withContext(Dispatchers.IO) {
            val request = createRequest(operation)
            client.newCall(request).execute().use { response ->
                if (response.isSuccessful) {
                    response.processResponse(modelClass)
                } else {
                    return@withContext Resource.Error()
                }
            }
        }
    } catch (ce: CancellationException) {
        throw ce
    } catch (t: Throwable) {
        Timber.e(t)
        Resource.Error(t)
    }

    private fun <M> Response.processResponse(modelClass: Class<M>): Resource<M> {
        val responseBody = this.body?.string().orEmpty()
        val type = Types.newParameterizedType(DataResponse::class.java, modelClass)
        val data = moshi.adapter<DataResponse<M>>(type)
            .fromJson(responseBody)
        return if (data != null)
            Resource.Success(data.data)
        else Resource.Error()
    }

    private fun createRequest(query: Operation<*>): Request {
        val body: String = buildJsonString { query.composeJsonRequest(this) }
        return Request.Builder()
            .url(apiUrl)
            .post(body.toRequestBody(JSON))
            .build()
    }
}